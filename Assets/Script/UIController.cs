﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//tell system we're going to be using UI controls
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    //define what image we're dealing with
    public Image Img1;
    public Text InputText;
    public Text OutputText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if user presses q
        if(Input.GetKeyDown("q"))
        {
            //disables image
            Img1.enabled = false;
        }
        if (Input.GetKeyDown("w"))
        {
            //enables image
            Img1.enabled = true;
        }
        //Unlocks cursor so you can use it
        //locked it in player lock script
        //Cursor.lockstate = CursorLockMode.None;

        //check if an input is a specific answer
        if (InputText.text == "no")
        {
            OutputText.text = "rude";
        }
        else if (InputText.text == "yes")
        {
            OutputText.text = "yay";
        }
        else
        {
            //displays user input in text
            OutputText.text = InputText.text;
        }
    }
}
